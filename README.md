# README #

Practica 3, busqueda de un camino en un grafo.

### comandos para la ejecución del programa ###

* mvn compile (compilar el programa maven)
* mvn test (comprobar que pasan los Test de forma correcta)


### Complejidad de los metodos ###

* addVertex:
        - complejidad temporal: complejidad lineal, 0(n)
        - complejidad espacial: complejidad constante, O(1)

* addEdge:
        - complejidad temporal: complejidad cuadratica,0(n2)
        - complejidad espacial: complejidad constante, 0(1)

* obtainAdjacents:
        - complejidad temporal: complejidad lineal, O(n)
        - complejidad espacial: complejidad constante,O(1)

* containsVertex:
        - complejidad temporal: complejidad constatne,O(1)
        - complejidad espacial: complejidad constante,O(1)

* toString:
        - complejidad temporal: complejidad lineal,0(n)
        - complejidad espacial: complejidad constante,0(1)

* onePath:
        - complejidad temporal:
        - complejidad espacial: complejidad exponencial,O(2n)


### Autor del repositorio ###

* Carlos Alonso Gradillas
