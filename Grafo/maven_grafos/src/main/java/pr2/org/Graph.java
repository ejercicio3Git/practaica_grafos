/* Copyright 2021 Carlos Alonso Gradillas

   Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License. */



package pr2.org;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class Graph<V> {

	private Map<V, Set<V>> adjacencyList = new HashMap<>();

/******************************************************************
* Añade el vértice ‘v‘ al grafo.
*
* @param v vértice a añadir.
* @return ‘true‘ si no estaba anteriormente y ‘false‘ en caso contrario.
******************************************************************/
	public boolean addVertex(V v) {
		if (adjacencyList.containsKey(v)) {
			return false;
		} else {
			adjacencyList.put(v, new HashSet<V>());
			return true;
		}
	}

/******************************************************************
* Añade un arco entre los vértices ‘v1‘ y ‘v2‘ al grafo. En caso de que no
* exista alguno de los vértices, lo añade también.
*
* @param v1 el origen del arco.
* @param v2 el destino del arco.
* @return ‘true‘ si no existía el arco y ‘false‘ en caso contrario.
******************************************************************/
	public boolean addEdge(V v1, V v2) {
		// Existen ambos vértices
		boolean existen = false;
		Set<V> adjacents = null;

		// Los creo. Si ya existen el método addVertex no los añade
		addVertex(v1);
		addVertex(v2);
		adjacents = adjacencyList.get(v1); // adjacents está el conjunto de los adyacentes a v1, todos con los que conecta v1
		if (!adjacents.contains(v2)) {
			adjacents.add(v2); // Si ya estuviese no lo mete pues es un conjunto y no admit repetidos
			adjacents = adjacencyList.get(v2);
			adjacents.add(v1);
			existen = true;
		}
		return existen;

	}

/******************************************************************
* Obtiene el conjunto de vértices adyacentes a ‘v‘.
*
* @param v vértice del que se obtienen los adyacentes.
* @return conjunto de vértices adyacentes.
******************************************************************/
	public Set<V> obtainAdjacents(V v) throws Exception {
		if (adjacencyList.containsKey(v)) {
			return adjacencyList.get(v);

		} else {
			throw new Exception("No existe el vertice.");
		}
	}

/******************************************************************
* Comprueba si el grafo contiene el vértice dado.
*
* @param v vértice para el que se realiza la comprobación.
* @return ‘true‘ si ‘v‘ es un vértice del grafo.
******************************************************************/
	public boolean containsVertex(V v) {
		return adjacencyList.containsKey(v);
	}

/******************************************************************
* Método ‘toString()‘ reescrito para la clase ‘Grafo.java‘.
*
* @return una cadena de caracteres con la lista de adyacencia .
******************************************************************/

	@Override
	public String toString() {
		String aux = "GRAFO\n";
		V next = null;
		Set<V> adjacents = null;
		Iterator<V> it = adjacencyList.keySet().iterator();
		while (it.hasNext()) {
			next = it.next(); // Se usa una vez por iteración del bucle
			aux += next + " : ";
			adjacents = adjacencyList.get(next);
			for (V v : adjacents) {
				aux += v.toString() + " ";
			}
			aux += "\n";
		}
		return aux;
	}

/******************************************************************
* Obtiene, en caso de que exista, un camino entre ‘v1‘ y ‘v2 ‘. En caso
* contrario, devuelve ‘null‘.
*
* @param v1 el vértice origen.
* @param v2 el vértice destino.
* @return lista con la secuencia de vértices desde ‘v1‘ hasta ‘v2‘ pasando por
*         arcos del grafo.
******************************************************************/

	public List<V> onePath(V v1, V v2) {
		Stack<V> abierta = new Stack<V>();
		ArrayList<V> traza = new ArrayList<V>();
		V v, aux;
		boolean encontrado;
		int cont;

		if (!adjacencyList.containsKey(v1) || !adjacencyList.containsKey(v2)) {
			return null;
		} else {
			abierta.push(v1);
			encontrado = false;
		//principio y final mismo vertice (TEST)??
		
		while (!abierta.isEmpty() && !encontrado) {
			v = abierta.pop();
			if (v.equals(v2)) {
			encontrado = true;
			traza.add(v);
		} else {
			cont = 0;
			Iterator<V> it = adjacencyList.get(v).iterator();
			while (it.hasNext()) {
				aux = it.next();
				if (!traza.contains(aux)) {
				abierta.push(aux);
				cont++;
				}
			}
			if (cont > 0) {
				traza.add(v);
			}
		}	
	}

	if (encontrado) {
		return traza;
	}
	return null; // Si existiendo los vértices NO encuentra un camino entre ellos
	}
	}
}














