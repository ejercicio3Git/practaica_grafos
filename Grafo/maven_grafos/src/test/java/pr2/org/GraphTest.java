/* Copyright 2021 Carlos Alonso Gradillas
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License. */

package pr2.org;

import java.util.*;
import org.junit.Assert.assertEquals;
import org.junit.Assert.assertTrue;
import org.junit.Assert.assertFalse;
import org.junit.Assert.assertNull;
import org.junit.Assert.assertNotNull;
import org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;



public class GraphTest{
	private Graph<Integer> myGraph;

	@Before
	public void Graph(){
		myGraph = new Graph<Integer>();
	}

	@Test
	public void graphExist(){
		assertNotNull(myGraph);
	}

	@Test public void addVertex(){
		boolean prueba = myGraph.addVertex(1);
		assertTrue(prueba);
	}

	@Test
	public void addSameVertex(){
		boolean prueba1 = myGraph.addVertex(1);
		boolean prueba2 = myGraph.addVertex(1);
		assertTrue(prueba1);
		assertFalse(prueba2);
	}

	@Test
	public void addEdge(){ //espera dos vertices
		myGraph.addVertex(1);
		myGraph.addVertex(2);
		boolean prueba = myGraph.addEdge(1,2);
		assertTrue(prueba);
	}

	@Test
	public void addOneEdge(){
		myGraph.addVertex(1);
		boolean prueba = myGraph.addEdge(1,2);
		assertFalse(prueba);
	}

	@Test
	public void obtainAdjecents(){ //espera dos vertices que esten conectados
		myGraph.addVertex(1);
		myGraph.addVertex(2);
		myGraph.addEdge(1,2);
		boolean prueba = myGraph.obtainsAdjacents(1).contains(2);
		assertTrue(prueba);
	}

	@Test
	public void obtainNoAdjacents(){
		myGraph.addVertex(1);
		myGraph.addVertex(3);
		myGraph.addEdge(1,2);
		boolean prueba = myGraph.obtainsAdjacents(1).contains(2);
		assertFalse(prueba);
	}
	
	@Test
    public void toStringVacio(){ 
        String expectedString = "Vertice\t Conexiones\n"; 
        assertEquals(expectedString, myGraph.toString());
    }
	
	@Test
    public void toStringOneVertex(){
        myGraph.addVertex(1);    
        String expectedString = "Vertice\t Conexiones\n" + "1\t\n";
        assertEquals(expectedString, myGraph.toString());
    }

	/*
	 * Este test comprueba que el método ‘onePath(V v1, V v2)‘
	 * encuentra un camino entre ‘v1‘ y ‘v2‘ cuando existe.
	 */

	@Test
	public void onePathFindsAPath(){
		System.out.println("\nTest onePathFindsAPath");
		System.out.println("----------------------");
		// Se construye el grafo.
		Graph<Integer> g = new Graph<>();
		g.addEdge(1, 2);
		g.addEdge(3, 4);
		g.addEdge(1, 5);
		g.addEdge(5, 6);
		g.addEdge(6, 4);

		// Se construye el camino esperado.
		List<Integer> expectedPath = new ArrayList<>();
		expectedPath.add(1);
		expectedPath.add(5);
		expectedPath.add(6);
		expectedPath.add(4);

		//Se comprueba si el camino devuelto es igual al esperado.
		assertEquals(expectedPath, g.onePath(1, 4));
	}
	
	@Test
	public void nullPath(){
		System.out.println("\nTest onePathFindsAPath");
                System.out.println("----------------------");
                // Se construye el grafo.
                Graph<Integer> g = new Graph<>();
                g.addEdge(1, 2);
                g.addEdge(3, 4);
                g.addEdge(1, 5);
                g.addEdge(5, 6);
                g.addEdge(6, 4);
		//vertice no encontrado
		assertNull(g.onePath(5,7));
	}

	@Test
	public void samePath(){
		System.out.println("\nTest onePathFindsAPath");
                System.out.println("----------------------");
                // Se construye el grafo.
                Graph<Integer> g = new Graph<>();
                g.addEdge(1, 2);
                g.addEdge(3, 4);
                g.addEdge(1, 5);
                g.addEdge(5, 6);
                g.addEdge(6, 4);
		 // Se construye el camino esperado.
                List<Integer> expectedPath = new ArrayList<>();
                expectedPath.add(1);
                //Se comprueba si el camino devuelto es igual al esperado.
                assertEquals(expectedPath, g.onePath(1, 1));
	}
	
}

